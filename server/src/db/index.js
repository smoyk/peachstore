const redis = require("redis");
const config = require("config");
const Promise = require("bluebird");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

/**
 * Create one instance of Redis db
 */
const database = redis.createClient(config.redis);
module.exports = database;
