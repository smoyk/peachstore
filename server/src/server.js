const Koa = require("koa");
const Router = require("koa-router");
const mount = require("koa-mount");
const parser = require("koa-bodyparser");
const helmet = require("koa-helmet");
const cors = require("koa-cors");

const app = new Koa();
const router = new Router();

app.keys = ["This is peachstore backend"];

const PORT = process.env.PORT || 1024;

app
  .use(cors())
  .use(router.allowedMethods())
  .use(helmet())
  .use(parser())
  .use(async (ctx, next) => {
    try {
      await next();
      //console.log("### after service call, response body: ", ctx.body);
    } catch (e) {
      ctx.status = e.status ? e.status : 500;
      console.log(">>> Error occured in service call, ", ctx.status, e.message);
      ctx.body = {
        error: true,
        message: e.message
      };
    }
  })
  .use(mount(require("./routes")));

app.use(router.routes());
const server = app.listen(PORT).on("error", err => {
  console.error(err);
});

module.exports = server;
