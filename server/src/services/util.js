const database = require("../db");

/**
 * Close database instace
 */
function closeDatabase() {
  console.log("************* redis close instance");
  database.quitAsync();
}

/**
 * Empty database
 */
function removeAllData() {
  database.flushdb();
}

module.exports.closeDatabase = closeDatabase;
