const database = require("../db");
const moment = require("moment");

/**
 * Create product
 * args: name, description, type and amount (all args are mandatory)
 * return: name,  description, type, amount, id and date
 * type should be from list ["plant", "digital", "veg", "cloth", "toy", "other"]
 * error: 400 if any required field are missing or in invalid shape
 */
module.exports.createProduct = async product => {
  console.log("inside service create", product);
  try {
    if (!product.id) product.id = await getId();
    product.date = moment().format("YYYY/MM/DD hh:mm:ss");
    const pattern = `goofyproducts:${product.id}`;
    console.log(`pattern ${pattern}`);
    const result = await database.hmsetAsync(
      pattern,
      "name",
      product.name,
      "description",
      product.description,
      "type",
      product.type,
      "id",
      product.id,
      "amount",
      product.amount,
      "date",
      product.date
    );
    return product;
  } catch (e) {
    throw e;
  }
};

/**
 * Get products returns all saved products in store
 * args : ()
 * return: {total:4, products:[{product}]}
 */
module.exports.getProducts = async (condition, offset = 0, limit = 50) => {
  try {
    const pattern = `goofyproducts:*`;
    console.log(`pattern ${pattern}`);
    const keys = await database.keysAsync(pattern);
    let products = [];
    for (let i = 0; i < keys.length; i++) {
      let product = await database.hgetallAsync(keys[i]);
      products.push(product);
    }
    return products;
  } catch (e) {
    throw e;
  }
};

/**
 * Get one product with provided productId
 * args: id (productId)
 * return: {name, description, type, amount, id, date}
 * error: 404 if there is no product with provided ip
 */
module.exports.getProduct = async productId => {
  try {
    const pattern = `goofyproducts:${productId}`;
    console.log(`pattern ${pattern}`);
    const product = await database.hgetallAsync(pattern);
    if (!product) return false;
    return product;
  } catch (e) {
    throw e;
  }
};

/**
 * Update one product with provided id
 * args: id (productId)
 * return: updatedProduct {name, description, type, amount, id, date}
 * error: 404 if product not found, 400 if validations violate.
 */
module.exports.updateProduct = async product => {
  try {
    const pattern = `goofyproducts:${product.id}`;
    const found = await database.hgetallAsync(pattern);
    if (!found) return false;

    product.date = moment().format("YYYY/MM/DD hh:mm:ss");
    await database.hmsetAsync(
      pattern,
      "name",
      product.name,
      "type",
      product.type,
      "id",
      product.id,
      "amount",
      product.amount,
      "description",
      product.description,
      "date",
      product.date
    );
    return product;
  } catch (e) {
    throw e;
  }
};

/**
 * Delete one prodict with provided id
 * args: id (productId)
 * return: 404 if product not found and 204 if product found
 */
module.exports.deleteProduct = async productId => {
  try {
    const pattern = `goofyproducts:${productId}`;
    console.log(`pattern ${pattern}`);
    const found = await database.hgetallAsync(pattern);
    if (found) {
      await database.delAsync(pattern);
      return true;
    }
    return false;
  } catch (e) {
    throw e;
  }
};

/**
 * Internal function that makes unique id for persistance
 */
const getId = async () => {
  console.log("####### getId");
  let _id = await database.getAsync("id");
  if (_id) {
    _id++;
  } else {
    _id = 1000;
  }
  await database.setAsync("id", _id);
  return _id;
};
