const Router = require("koa-router");
const Koa = require("koa");

const Services = require("../services");
const validate = require("./validate");

const app = new Koa();
const router = new Router();
router.post("/products", validate.validateCreateProduct, async ctx => {
  const body = ctx.request.body;
  console.log("### Try to add product", JSON.stringify(body));

  try {
    const product = await Services.createProduct(body);
    ctx.body = product;
  } catch (e) {
    throw e;
  }
});

router.get("/products", async ctx => {
  console.log("### Try to get all products");
  const condition = ctx.request.query;
  const offset = ctx.request.query.offset;
  const limit = ctx.request.query.limit;
  try {
    let products = await Services.getProducts(condition, offset, limit);

    if (!products) products = [];
    const total = products.length;
    ctx.res.setHeader("Access-Control-Allow-Origin", "*");

    ctx.body = { products, total };
  } catch (e) {
    throw e;
  }
});

router.get("/products/:id", async ctx => {
  const productId = ctx.params.id;
  console.log(`Try to get product with id ${productId}`);
  try {
    const product = await Services.getProduct(productId);
    console.log(`product found ${product.name}`);
    if (product === false) {
      ctx.status = 404;
      ctx.body = { error: true, message: "product not found" };
    } else {
      ctx.body = product;
    }
  } catch (e) {
    throw e;
  }
});

router.put("/products/:id", validate.validateUpdateProduct, async ctx => {
  const product = ctx.request.body;
  product["id"] = ctx.params.id;
  console.log(`Try to update product ${JSON.stringify(product)}`);
  try {
    const result = await Services.updateProduct(product);
    if (result !== false) {
      ctx.status = 200;
      ctx.body = product;
    } else {
      ctx.status = 404;
      ctx.body = { error: true, message: "product not found" };
    }
  } catch (e) {
    throw e;
  }
});

router.del("/products/:id", validate.validateDeleteProduct, async ctx => {
  const productId = ctx.params.id;
  console.log(`Try to delete product with id ${productId}`);
  try {
    const result = await Services.deleteProduct(productId);
    ctx.response.set("Access-Control-Allow-Origin", "*");
    if (result === true) {
      ctx.status = 204;
    } else {
      ctx.status = 404;
      ctx.body = { error: true, message: "product not found" };
    }
  } catch (e) {
    throw e;
  }
});
app.use(router.routes());
module.exports = app;
