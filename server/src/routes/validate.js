module.exports.validateCreateProduct = async (ctx, next) => {
  const body = ctx.request.body;
  ctx.assert(ctx.is("json"), 400, "content type should be json");
  ctx.assert(body, 400, "invalid body");
  ctx.assert(validateProductName(body.name), 400, "invalid name");
  ctx.assert(
    validateProductDescription(body.description),
    400,
    "invalid description"
  );
  ctx.assert(validateAmount(body.amount), 400, "invalid amount");
  ctx.assert(validateProductType(body.type), 400, "invalid type");
  await next();
};

module.exports.validateUpdateProduct = async (ctx, next) => {
  const body = ctx.request.body;
  ctx.assert(ctx.is("json"), 400, "content type should be json");
  ctx.assert(validateProductName(body.name), 400, "invalid name");
  ctx.assert(
    validateProductDescription(body.description),
    400,
    "invalid description"
  );
  ctx.assert(validateAmount(body.amount), 400, "invalid amount");
  ctx.assert(validateProductType(body.type), 400, "invalid type");
  await next();
};

module.exports.validateDeleteProduct = async (ctx, next) => {
  ctx.assert(ctx.params.id, 400, "invalid id");
  await next();
};

const validateProductName = productName => {
  return productName && /^[A-Za-z 0-9 _]{1,30}$/.test(productName);
};

const validateProductDescription = description => {
  return description && /^[A-Za-z 0-9 _]{1,100}$/.test(description);
};

const validateProductType = productType => {
  const validTypes = ["plant", "digital", "veg", "cloth", "toy", "other"];
  return validTypes.includes(productType);
};

const validateAmount = amount => {
  return amount && /^\d+$/.test(amount);
};
