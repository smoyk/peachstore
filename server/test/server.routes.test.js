// require the Koa server
const server = require("../src/server");
//const router = require("../src/routes");
// require the services
const util = require("../src/services/util");

// require supertest
const request = require("supertest");

// const a = new myMock();
// const b = {};
beforeEach(() => {
  initializePeachDatabase();
});

afterEach(() => {});

beforeAll(function() {});

afterAll(() => {
  util.closeDatabase();
  server.close();
});

initializePeachDatabase = function() {
  const product_0 = {
    name: "cellphone iphone7",
    type: "digital",
    description: "second hand cellphone 7 availabe in shop",
    amount: "650",
    id: "999"
  };
  const product_1 = {
    name: "tshirt 84",
    type: "cloth",
    description: "black and white small size tshirt with red 1384 on it",
    amount: "27",
    id: "1001"
  };
  //const resp = await request(server).post("/products").send(product_0).set("Content-Type", "application/json")
};

describe("routes: POST /products", () => {
  const product = {
    name: "cellphone iphone7",
    type: "digital",
    description: "second hand cellphone 7 availabe in shop",
    amount: "650",
    id: "999"
  };

  test("should respond 400 if there is no content type", async () => {
    const response = await request(server)
      .post("/products")
      .send({});
    expect(response.status).toEqual(400);
  });

  test("should respond content type should be json if there is no content type", async () => {
    const response = await request(server)
      .post("/products")
      .send();
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("content type should be json");
  });

  test("should respond 400 if there is no name", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({});
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid name");
  });

  test("should respond 400 if there is invalid characters in name", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({ name: "Sa!!!!!*mp#le@produc<t>" });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid name");
  });

  test("should respond 400 if there is invalid characters in name", async () => {
    const veryLongName = "veryyyyy veryyyyy longggggggggg nameeeeeeeeee";
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({
        name: veryLongName
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid name");
  });

  test("should respond 400 if there is no description", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({ name: product.name });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid description");
  });

  test("should respond 400 if there is invalid characters in description", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({ name: product.name, description: "Sa*mp#le @produc<t>" });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid description");
  });

  test("should respond 400 if there is no amount", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({ name: product.name, description: product.description });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid amount");
  });

  test("should respond 400 if there is invalid characters in amount", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({
        name: product.name,
        description: product.description,
        amount: "abcd"
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid amount");
  });
  test("should respond 400 if there is no type", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({
        name: product.name,
        description: product.description,
        amount: product.amount
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid type");
  });

  test("should respond 400 if there is unknown type", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send({
        name: product.name,
        description: product.description,
        amount: product.amount,
        type: "car"
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid type");
  });
  test("should respond as expected", async () => {
    const response = await request(server)
      .post("/products")
      .set("Content-Type", "application/json")
      .send(product);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.id).toEqual("999");
    expect(response.body.name).toEqual(product.name);
    expect(response.body.type).toEqual(product.type);
    expect(response.body.description).toEqual(product.description);
    expect(response.body.amount).toEqual(product.amount);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining([
        "name",
        "description",
        "type",
        "amount",
        "id",
        "date"
      ])
    );
  });
});

describe("routes: GET /products", () => {
  test("should respond as expected", async () => {
    const response = await request(server).get("/products");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["products", "total"])
    );
  });

  test("should return more than 0 products", async () => {
    const response = await request(server).get("/products");
    expect(response.body.total).toBeGreaterThan(0);
  });

  test("should return product details", async () => {
    const response = await request(server).get("/products");
    expect(Object.keys(response.body.products[0])).toEqual(
      expect.arrayContaining([
        "name",
        "description",
        "type",
        "amount",
        "id",
        "date"
      ])
    );
  });
});

describe("routes: GET /products/:id", () => {
  test("should respond as expected", async () => {
    const response = await request(server).get("/products/999");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(6);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining([
        "name",
        "description",
        "type",
        "amount",
        "id",
        "date"
      ])
    );
    expect(response.body.id).toEqual("999");
  });

  test("should respond 404 if product not found", async () => {
    const response = await request(server).get("/products/1");
    expect(response.status).toEqual(404);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("product not found");
  });
});

describe("routes: PUT /products/:id", () => {
  const product = {
    name: "cellphone iphone7",
    type: "digital",
    description: "second hand cellphone 7 availabe in shop",
    amount: "650",
    id: "999"
  };

  const new_product = {
    name: "cellphone sumsung",
    type: "digital",
    description: "new sumsung cellphone",
    amount: "480"
  };
  test("should respond 400 if there is no content type", async () => {
    const response = await request(server)
      .put("/products/999")
      .send({});
    expect(response.status).toEqual(400);
  });

  test("should respond content type should be json if there is no content type", async () => {
    const response = await request(server)
      .put("/products/999")
      .send();
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("content type should be json");
  });
  test("should respond 404 if product not found", async () => {
    const response = await request(server)
      .put("/products/10150")
      .send(product)
      .set("Content-Type", "application/json");
    expect(response.status).toEqual(404);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("product not found");
  });

  test("should respond 400 if there is no name", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({});
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid name");
  });

  test("should respond 400 if there is invalid characters in name", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({ name: "Sa!!!!!*mp#le@produc<t>" });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid name");
  });

  test("should respond 400 if there is invalid characters in name", async () => {
    const veryLongName = "veryyyyy veryyyyy longggggggggg nameeeeeeeeee";
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({
        name: veryLongName
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid name");
  });

  test("should respond 400 if there is no description", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({ name: product.name });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid description");
  });

  test("should respond 400 if there is invalid characters in description", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({ name: product.name, description: "Sa*mp#le @produc<t>" });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid description");
  });

  test("should respond 400 if there is no amount", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({ name: product.name, description: product.description });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid amount");
  });

  test("should respond 400 if there is invalid characters in amount", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({
        name: product.name,
        description: product.description,
        amount: "abcd"
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid amount");
  });
  test("should respond 400 if there is no type", async () => {
    const response = await request(server)
      .put("/products/1015")
      .set("Content-Type", "application/json")
      .send({
        name: product.name,
        description: product.description,
        amount: product.amount
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid type");
  });

  test("should respond 400 if there is unknown type", async () => {
    const response = await request(server)
      .put("/products/999")
      .set("Content-Type", "application/json")
      .send({
        name: product.name,
        description: product.description,
        amount: product.amount,
        type: "car"
      });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("invalid type");
  });

  test("should respond as expected", async () => {
    const response = await request(server)
      .put("/products/999")
      .set("Content-Type", "application/json")
      .send(new_product);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.id).toEqual("999");
    expect(response.body.name).toEqual(new_product.name);
    expect(response.body.type).toEqual(new_product.type);
    expect(response.body.description).toEqual(new_product.description);
    expect(response.body.amount).toEqual(new_product.amount);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining([
        "name",
        "description",
        "type",
        "amount",
        "id",
        "date"
      ])
    );
  });
});

describe("routes: DEL /products/:id", () => {
  test("should respond as expected", async () => {
    const response = await request(server).del("/products/999");
    expect(response.status).toEqual(204);
  });

  test("should respond 404 if product not found", async () => {
    const response = await request(server).get("/products/10");
    expect(response.status).toEqual(404);
    expect(response.type).toEqual("application/json");
    expect(Object.keys(response.body)).toHaveLength(2);
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["error", "message"])
    );
    expect(response.body.error).toBeTruthy();
    expect(response.body.message).toEqual("product not found");
  });
});
