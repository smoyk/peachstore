# peachStore

It presents simple crud functionality with nodejs and react.

# This is a goofyapp backend to provide services api.

- How to run services?

* Make sure you have redis installed and run
* Change redis config in config/production.yaml
* inside the path peachstore/server run command `make dev`
* services will be available at `localhost:1024`

### GET {base_address}/products

- **note:** Returns all available products

#### Results Format

##### Successful result format (status code 200)

```
{
    "products": [
        {
            "name": "pro2",
            "description": "peo 2 desc",
            "type": "veg",
            "id": "1088",
            "amount": "2",
            "date": "2018/10/23 07:15:54"
        },
        {
            "name": "Hi",
            "description": "sample product desc",
            "type": "plant",
            "id": "1087",
            "amount": "12",
            "date": "2018/10/23 07:15:33"
        }
    ],
    "total": 2
}
```

### POST /products

- **note:** Create one product

#### Body parameters

- **name:** 'sample product name'
- mandatory, should be string, max length:30, name coul have a-z A-Z 0-9 - \_
- **description:** 'smple product descrition and this could be long text'
- mandatory, should be string, max length:100, descrition coul have a-z A-Z 0-9 - \_
- **amount:** 1200
- mandatory, should be digits
- **type:** 'toy'
- mandatory, should be string and from this list ["plant", "digital", "veg", "cloth", "toy", "other"]

- sample body:

```
{ "name": "product_cellphone"
, "description": "this is new brand cellphone and will be available after first og aug"
, "type": "digital"
, "amount": "1300"
}
```

#### Results Format

##### Successful result format (status code 200)

- sample success body:

```
{ "name": "product_cellphone"
, "description": "this is new brand cellphone and will be available after first og aug"
, "type": "digital"
, "amount": "1300"
, "date": "2018/09/11 07:10:12"
, "id": "1001"
}
```

#### UnSuccessful result format (status code 400)

- all fields are mandatory
- if any filed is invalid server will return 400 with message `invalid {field}`
- sample error body:

```
{ "error": true
, "message": "invalid productName"
}
```

### GET /products/:id

- **note:** Get one product with provided id

#### URI Parameters

- **id:** productId
- mandatory

#### Results Format

##### Successful result format (status code 200)

- sample success body:

```
{ "name": "pro2"
, "description": "peo 2 desc"
, "type": "veg"
, "id": "1088"
, "amount": "2"
, "date": "2018/10/23 07:15:54"
}
```

### PUT /products/:id

- **note:** Update one existing product

#### URI Parameters

- **id:** productId
- mandatory

#### Body parameters

- **name:** 'sample product name'
- mandatory, should be string, max length:30, name coul have a-z A-Z 0-9 - \_
- **description:** 'smple product descrition and this could be long text'
- mandatory, should be string, max length:100, descrition coul have a-z A-Z 0-9 - \_
- **amount:** 1200
- mandatory, should be digits
- **type:** 'toy'
- mandatory, should be string and from this list ["plant", "digital", "veg", "cloth", "toy", "other"]

- sample body:

```
{ "name":"new product name"
, "description":"new product description"
, "type":"toy"
, "amount":"1000"
}
```

##### Successful result format (status code 200)

- sample success body:

```
{ "name": "new product name"
, "description": "new product description"
, "type": "toy"
, "amount": "1000"
, "id": "1088"
, "date": "2018/10/23 07:15:54"
}
```

### DEL /products/:id

- **note:** Delete one existing produnt

#### URI Parameters

- **id:** productId
- mandatory

##### Successful result format (status code 204)

##### UnSuccessful result format (status code 404)

- You can see sample ui that calls these services in peachShop[https://gitlab.com/smoyk/peachshop]
